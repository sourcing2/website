/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    protocol: 'https://',
    baseUrl: 'backend-nine-omega.vercel.app/'
  },
}

module.exports = nextConfig
